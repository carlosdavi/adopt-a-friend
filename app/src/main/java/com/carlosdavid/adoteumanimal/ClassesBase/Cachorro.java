package com.carlosdavid.adoteumanimal.ClassesBase;

import com.carlosdavid.adoteumanimal.R;

/**
 * Created by Carlos David on 13/02/2016.
 */
public class Cachorro extends Animal {
    String raca;
    String temperamento;
    String cor;

    @Override
    public int getAnimalImagem(int position){
        switch (position)
        {
            case 0:
                return R.drawable.cachorro1;
            case 1:
                return R.drawable.cachorro2;
            case 2:
                return R.drawable.cachorro3;
            case 3:
                return R.drawable.cachorro4;
            case 4:
                return R.drawable.cachorro5;
            case 5:
                return R.drawable.cachorro6;
            case 6:
                return R.drawable.cachorro7;
            case 7:
                return R.drawable.cachorro8;
            case 8:
                return R.drawable.cachorro9;
            case 9:
                return R.drawable.cachorro10;
            case 10:
                return R.drawable.cachorro11;
            case 11:
                return R.drawable.cachorro12;
            case 12:
                return R.drawable.cachorro13;
            default:
                return R.drawable.cachorro14;
        }


    }





}
