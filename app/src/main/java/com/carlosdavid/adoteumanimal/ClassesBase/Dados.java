package com.carlosdavid.adoteumanimal.ClassesBase;

import java.util.ArrayList;

/**
 * Created by Carlos David on 14/02/2016.
 */
public class Dados {
    ArrayList<String> nome = new ArrayList<>();
    ArrayList<String> descricao = new ArrayList<>();
   public Dados(){
       CarregarDados();
   }

    public void CarregarDados(){
        nome.add("João");
        nome.add("Zeus");
        nome.add("Zureia");
        nome.add("Polly");
        nome.add("Duck");
        nome.add("Napolion");
        nome.add("Leci");
        nome.add("Rintintin");
        nome.add("Mingal");
        nome.add("Biruta");
        nome.add("Nenem");
        nome.add("Lola");
        nome.add("Bob");

        descricao.add(" é um cão super dócil que procura um lar");
        descricao.add(" é um cão super dócil que procura um lar");
        descricao.add(" é um cão super dócil que procura um lar");
        descricao.add(" é um cão super dócil que procura um lar");
        descricao.add(" é um cão super dócil que procura um lar");
        descricao.add(" é um cão super dócil que procura um lar");
        descricao.add(" é um cão super dócil que procura um lar");
        descricao.add(" é um cão super dócil que procura um lar");
        descricao.add(" é um cão super dócil que procura um lar");
        descricao.add(" é um cão super dócil que procura um lar");
        descricao.add(" é um cão super dócil que procura um lar");
        descricao.add(" é um cão super dócil que procura um lar");
        descricao.add(" é um cão super dócil que procura um lar");
    }
    public void setDados(ArrayList<Animal> lista){
        for(int i =0; i<lista.size();i++)
        {
            Animal animal = new Animal();
            animal.nome = nome.get(i);
            animal.descricao= descricao.get(i);
            lista.add(i,animal);

        }

    }


}
