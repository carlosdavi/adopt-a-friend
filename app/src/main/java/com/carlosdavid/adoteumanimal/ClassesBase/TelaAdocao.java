package com.carlosdavid.adoteumanimal.ClassesBase;

import android.app.ActionBar;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.carlosdavid.adoteumanimal.R;

import java.util.ArrayList;
import java.util.List;

public class TelaAdocao extends FragmentActivity {
    ArrayList<String> nome = new ArrayList<>();
    ArrayList<String> descricao = new ArrayList<>();
    ArrayList<Animal> lista = new ArrayList<Animal>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CarregarDados();
        //layout

        ActionBar actionBar = getActionBar();
        //actionBar.setTitle("Adote");

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.backgroundcolor));

       // ---


        setContentView(R.layout.activity_tela_adocao);

        for(int i =0; i<13;i++)
        {
            Animal animal = new Animal();
            animal.nome = nome.get(i);
            animal.descricao= descricao.get(i);
            lista.add(i,animal);

        }

        ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,lista);
        ListView listView = (ListView)findViewById(R.id.listViewAnimais);
        listView.setAdapter(new AnimalAdapter(TelaAdocao.this, lista));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent = new Intent(TelaAdocao.this,DetalhesAnimais.class);
                Bundle bundle = new Bundle();
                bundle.putInt("position",position);
                bundle.putString("nome", lista.get(position).getNome());
                bundle.putString("descricao", lista.get(position).getDescricao());
                bundle.putString("endereco","Rua XYZ, Fortaleza");
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });


    }

    public void CarregarDados(){
        nome.add("João");
        nome.add("Zeus");
        nome.add("Zureia");
        nome.add("Polly");
        nome.add("Duck");
        nome.add("Napolion");
        nome.add("Leci");
        nome.add("Rintintin");
        nome.add("Mingal");
        nome.add("Biruta");
        nome.add("Nenem");
        nome.add("Lola");
        nome.add("Bob");

        descricao.add(" é um cão super dócil que procura um lar ");
        descricao.add(" é um cão super dócil que procura um lar ");
        descricao.add(" é um cão super dócil que procura um lar ");
        descricao.add(" é um cão super dócil que procura um lar");
        descricao.add(" é um cão super dócil que procura um lar ");
        descricao.add(" é um cão super dócil que procura um lar ");
        descricao.add(" é um cão super dócil que procura um lar ");
        descricao.add(" é um cão super dócil que procura um lar ");
        descricao.add(" é um cão super dócil que procura um lar ");
        descricao.add(" é um cão super dócil que procura um lar ");
        descricao.add(" é um cão super dócil que procura um lar ");
        descricao.add(" é um cão super dócil que procura um lar ");
        descricao.add(" é um cão super dócil que procura um lar ");
    }
    public void setDados(){
        for(int i =0; i<lista.size();i++)
        {
            Animal animal = new Animal();
            animal.nome = nome.get(i);
            animal.descricao= descricao.get(i);
            lista.add(i,animal);

        }
}

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {

        getMenuInflater().inflate(R.menu.layoutactionbar,menu);
        return  true;
    }





}
