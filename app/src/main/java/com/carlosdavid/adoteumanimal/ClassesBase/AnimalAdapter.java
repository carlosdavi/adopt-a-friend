package com.carlosdavid.adoteumanimal.ClassesBase;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.carlosdavid.adoteumanimal.R;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by Carlos David on 13/02/2016.
 */
public class AnimalAdapter extends android.widget.BaseAdapter {
    Context context;
    ArrayList<Animal> lista;

    public AnimalAdapter(Context context, ArrayList<Animal> lista)
    {
        this.context = context;
        this.lista = lista;

    }




    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int i) {
        return lista.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        Animal animal = lista.get(position);
        View layout;

        if(view == null)
        {

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layout = inflater.inflate(R.layout.layout,null);


        }
        else
        {
            layout = view;
        }
        ImageView imagemAnimal = (ImageView)layout.findViewById(R.id.imageView1);
        TextView nomeAnimal = (TextView)layout.findViewById(R.id.idNome);
        TextView descricaoAnimal = (TextView)layout.findViewById(R.id.idDescricao);

        imagemAnimal.setImageResource(animal.getAnimalImagem(position));
        nomeAnimal.setText(animal.getNome());
        descricaoAnimal.setText(animal.getDescricao());





        return layout;
    }
}
