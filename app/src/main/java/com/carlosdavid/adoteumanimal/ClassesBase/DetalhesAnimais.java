package com.carlosdavid.adoteumanimal.ClassesBase;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.carlosdavid.adoteumanimal.R;

public class DetalhesAnimais extends FragmentActivity {

    Intent intent;
    Bundle bundle;
    Animal animal = new Animal();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes_animais);

          intent = getIntent();
        bundle = intent.getExtras();

        ImageView imagem = (ImageView)findViewById(R.id.imageViewFotoDetalhes);
        TextView textoNome = (TextView)findViewById(R.id.textViewNomeDetalhes);
        TextView detalhes= (TextView)findViewById(R.id.textSobre);
        TextView ondeMeEncontrar= (TextView)findViewById(R.id.textLocal);

        imagem.setImageResource(animal.getAnimalImagem(bundle.getInt("position")));
        textoNome.setText(bundle.getString("nome"));
        detalhes.setText(detalhes.getText() + "\n" +bundle.getString("descricao"));
        ondeMeEncontrar.setText(ondeMeEncontrar.getText() + "\n" +bundle.getString("endereco"));



    }
}
