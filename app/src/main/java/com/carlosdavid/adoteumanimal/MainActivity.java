package com.carlosdavid.adoteumanimal;

import android.app.ActionBar;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

import com.carlosdavid.adoteumanimal.ClassesBase.TelaAdocao;

public class MainActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar actionBar = getActionBar();
        actionBar.setTitle("Adote um Amigo");
        actionBar.setLogo(R.drawable.icone);

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.backgroundcolor));


    }

    public void ChamaAdocao(View view)
    {
        Intent intent = new Intent(MainActivity.this, TelaAdocao.class);
        startActivity(intent);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {

        getMenuInflater().inflate(R.menu.layoutactionbar,menu);
        return  true;
    }
}
